<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

Route::get('/export/{brand_id?}', 'IndexController@export')->name('export_excel');
//Route::match(['get', 'post'], '/import', 'IndexController@import')->name('import_excel');
Route::get('/import', ['uses' => 'IndexController@getImport', 'as' => 'get_import_excel']);
Route::post('/import', ['uses' => 'IndexController@postImport', 'as' => 'post_import_excel'])->middleware('demo');

Route::get('/', ['uses' => 'IndexController@home', 'as' => 'home']);
Route::group(['prefix' => 'brand-list'], function () {

    Route::get('/', ['uses' => 'IndexController@getBrandList', 'as' => 'get_brand_list']);
    Route::post('/', ['uses' => 'IndexController@postBrandList', 'as' => 'post_brand_list'])->middleware('demo');

    Route::get('/edit', ['uses' => 'IndexController@getEditBrand', 'as' => 'get_brand_edit']);
    Route::post('/edit', ['uses' => 'IndexController@postEditBrand', 'as' => 'post_brand_edit'])->middleware('demo');
    Route::get('/mapping', ['uses' => 'IndexController@mapping_result', 'as' => 'mapping_result']);
    Route::get('/mapping-fail/{brand_id?}', ['uses' => 'IndexController@mapping_fail', 'as' => 'mapping_fail']);
    Route::get('/panel/{id?}', ['uses' => 'IndexController@brand_panel', 'as' => 'brand_panel']);

    Route::get('mapping/edit', ['uses' => 'IndexController@getEditMapping', 'as' => 'get_mapping_edit']);
    Route::post('mapping/edit', ['uses' => 'IndexController@postEditMapping', 'as' => 'post_mapping_edit'])->middleware('demo');

});

Route::group(['prefix' => 'debug'], function () {
    Route::get('selenium', function () {
        $driver = \App\Service\DriverService::getDriver('chrome_headless');
        $driver->get('https://www.google.com/');
        $res = $driver->getPageSource();
        $driver->quit();
        return 'Selenium is running';
    });
});


