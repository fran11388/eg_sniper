<?php

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('首頁', route('home'));
});

// Home > brand_list
Breadcrumbs::for('brand_list', function ($trail) {
    $trail->parent('home');
    $trail->push('品牌清單', route('get_brand_list'));
});

// Home > brand_list > brand_panel
Breadcrumbs::for('brand_panel', function ($trail,$brand_id) {
    $trail->parent('brand_list');
    $trail->push('品牌總攬', route('brand_panel',$brand_id));
});

// Home > brand_list > brand_panel >mapping_result
Breadcrumbs::for('mapping_result', function ($trail,$brand_id) {
    $trail->parent('brand_panel',$brand_id);
    $trail->push('比對結果', route('mapping_result'));
});

// Home > brand_list > brand_panel >mapping_result
Breadcrumbs::for('mapping_edit', function ($trail,$brand_id) {
    $trail->parent('brand_panel',$brand_id);
    $trail->push('mapping_edit', route('get_mapping_edit'));
});

