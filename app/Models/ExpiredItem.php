<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ExpiredItem extends Model
{
    protected $connection = 'eg_sniper';
    protected $table = 'expired_items';
    protected $guarded = [];
}
