<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemChangeLog extends Model
{
    protected $connection = 'eg_sniper';
    protected $table = 'outer_items_change_log';
    protected $guarded = [];
}
