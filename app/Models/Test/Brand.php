<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/5/10
 * Time: 下午 03:24
 */

namespace App\Models\Test;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'test_brands';
    protected $guarded = [];
}