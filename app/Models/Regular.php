<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Regular extends Model
{
    protected $connection = 'eg_sniper';
    protected $table = 'regulars';
    protected $guarded = [];
}
