<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MappingProduct extends Model
{
    protected $connection = 'eg_sniper';
    protected $table = 'mapping_products';
    protected $guarded = [];

    public function outerItem(){
        return $this->belongsTo('\App\Models\OuterItem','outer_item_id');
    }

    public function status(){
        return $this->hasOne('\App\Models\Status','id','status_id');
    }


}
