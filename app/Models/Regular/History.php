<?php

namespace App\Models\Regular;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $connection = 'eg_sniper';
    protected $table = 'regular_histories';
    protected $guarded = [];
}
