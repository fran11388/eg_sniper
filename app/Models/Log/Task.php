<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/4/9
 * Time: 下午 05:10
 */

namespace App\Models\Log;

use Illuminate\Database\Eloquent\Model;
class Task extends Model
{
    protected $connection = 'eg_sniper';
    protected $table = 'task_logs';
    protected $guarded = [];
}