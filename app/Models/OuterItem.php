<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OuterItem extends Model
{
    protected $connection = 'eg_sniper';
    protected $table = 'outer_items';
    protected $guarded = [];

    public function mappingProducts(){
        return $this->hasMany('App\Models\MappingProduct');
    }

    public function changeLogs(){
        return $this->hasMany('App\Models\ItemChangeLog');
    }
}
