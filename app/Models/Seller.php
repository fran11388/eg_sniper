<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Seller extends Model
{
    protected $connection = 'eg_sniper';
    protected $table = 'sellers';
    protected $guarded = [];

    public function items(){
        return $this->hasMany('App\Models\OuterItem');
    }
}
