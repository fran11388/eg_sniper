<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Status extends Model
{
    protected $connection = 'eg_sniper';
    protected $table = 'status';
    protected $guarded = [];
}
