<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/4/9
 * Time: 下午 05:37
 */

namespace App\Constants;


class Command
{
    const CRAWL_SELLER = 'product:crawl';
    const MAPPING_PRODUCT = 'product:map';
    const CALCULATE_PRICE = 'product:calculate';
    const APPLY_PRICE = 'apply:price';
    const RESET_STATUS = 'reset:status';
    const DELETE_EXPIRED = 'delete:expired';
    const SYNC_PRICE = 'sync:price';
}