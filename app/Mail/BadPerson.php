<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class BadPerson extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $from_who='Frank Yan';
        $reason="This is bad person feature test";
        return $this->from([
//            'address' => 'someone@webike.tw',
            'address' => 'taipei_erp@everglory.asia',
            'name' => 'Frank',
        ])->subject("[Bad Person!] You was chosen by $from_who " . date('Y-m-d'))
//            ->markdown('emails.orders.shipped');
            ->view('emails.trick.trick')->with([
                'from_who' => $from_who,
                'reason' => $reason,
            ]);

    }
}
