<?php

namespace App\Mail;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Notify extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {

        $yesterday = Carbon::yesterday();
        $yesterday = $yesterday->toDateString();

        $query = \DB::select("SELECT
	( SELECT count( * ) AS '昨日新增品牌' FROM ( SELECT * FROM `sellers` WHERE created_at > '$yesterday' GROUP BY manufacturer_id ) AS sub_query ) AS '昨日新增品牌',
	( SELECT count( * ) AS '目前Total品牌' FROM ( SELECT * FROM `sellers` GROUP BY manufacturer_id ) AS sub_query ) AS '目前Total品牌',
	( SELECT count( * ) AS '昨日新增對應商品數' FROM ( SELECT * FROM mapping_products WHERE created_at > '$yesterday' ) AS sub_query ) AS '昨日新增對應商品數',
	( SELECT count( * ) AS '目前Total對應商品數' FROM ( SELECT * FROM mapping_products  ) AS sub_query ) AS '目前Total對應商品數',
	( SELECT count( * ) AS '有競爭力商品數' FROM ( SELECT * FROM mapping_products where status_id= 9 ) AS sub_query ) AS '有競爭力商品數',
	( SELECT count( * ) AS '免強有競爭力商品數' FROM ( SELECT * FROM mapping_products where status_id= 10 ) AS sub_query ) AS '免強有競爭力商品數',
	( SELECT count( * ) AS '無競爭力商品數' FROM ( SELECT * FROM mapping_products where status_id= 11 ) AS sub_query ) AS '無競爭力商品數',
	( SELECT count( * ) AS '未確認' FROM ( SELECT * FROM mapping_products where status_id= 13 ) AS sub_query ) AS '未確認'");

        $today=date("m-d");
        return $this->view('emails.notify.notify')
            ->subject("【EG】$today Sniper daily report")
            ->with([
                'query' => $query[0],

            ]);

    }
}
