<?php

namespace App\Http\Controllers;

use App\Console\Commands\ApplyPrice;
use App\Models\Log\Task;
use App\Models\MappingProduct;
use App\Service\Crawler\Shopee;
use App\Service\DriverService;
use App\Service\Status;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


use App\Service\MappingService;
use App\Service\ExcelService;

class IndexController extends Controller
{

    protected $mappingService;

    public function __construct(MappingService $mappingService)
    {
//        $this->middleware('auth');
        $this->mappingService = $mappingService;
    }

    public function home()
    {
        return view('home');
    }

    public function export($brand_id)
    {

//        ExcelService::export('aa', 'aa', [['a','b','c'],[4,5,6],[9,8,7]]);


        $cellData = [
            [
                '對手商品編號',
                '對手商品名稱',
                '對手價格',
                '對手商品網址',
                'solr關鍵字',
                '商品名稱',
                '商品編號',
                'C售價',
                'D售價',
                '新C售價',
                '新D售價',
                '網址',
                '比對結果編號',
                'sku',
                '商品狀態',
            ],
        ];


        $outer_items = \App\Models\OuterItem::with('mappingProducts.status')
            ->where('manufacturer_id', $brand_id)
            ->get();

        if (count($outer_items) == 0) dd('無商品可下載');
        foreach ($outer_items as $outer_item) {
            if (count($outer_item->mappingProducts) == 0) {
                dd('商品未對應完畢，禁止下載');
            }
            foreach ($outer_item->mappingProducts as $mappingProduct) {
                if ($mappingProduct->status_id == Status::WAITING_UPDATE) {
                    dd('SKU未更新完畢，禁止下載');
                }
            }

        }


        foreach ($outer_items as $outer_item) {
            foreach ($outer_item->mappingProducts as $mappingProduct) {
                $cellData[] = [
                    $outer_item->id,
                    ExcelService::replace_4byte($outer_item->name),
                    $outer_item->price_min,
                    $outer_item->url,
                    $mappingProduct->solr_keyword,
                    $mappingProduct->webike_name,
                    $mappingProduct->model_number,
                    $mappingProduct->webike_price_c,
                    $mappingProduct->webike_price_d,
                    $mappingProduct->webike_price_c_new,
                    $mappingProduct->webike_price_d_new,
                    $mappingProduct->webike_item_url,
                    $mappingProduct->id,
                    $mappingProduct->sku,
                    $mappingProduct->status->display_name
                ];
            }
        }

        try {
            $brand_query = file_get_contents(config('api.brand_query') . '?brand_ids=' . $brand_id);
            $brand_query = json_decode($brand_query);
            $brand_name = $brand_query[0]->title;
        } catch (\Exception $e) {
            $message = "brand query api get some problem: " . $e->getMessage();
            \Log::error($message);
            $brand_name = $brand_id;
        }

        ExcelService::export($brand_name, $brand_name, $cellData);
    }


    public function getImport(){
        return view('upload', ['name' => '']);
    }

    public function postImport(){
        echo collect(request()->file())->first()->getClientOriginalName() . " 開始上傳(" . date("Y-m-d H:i:s") . ")<br>";
        $excel_rows = ExcelService::readFirstUploadExcel();
        if ($excel_rows === false) return redirect()->back()->with('error', '請使用 .xls上傳');
        $excel_rows->map(function ($item, $key) {
            $item['sku'] = (string)$item['sku'];
            $item['對手商品編號'] = (int)$item['對手商品編號'];
            $item['比對結果編號'] = (int)$item['比對結果編號'];
            return $item;
        });

        //eager load
        $webike_products = $this->mappingService->getProductBySkus($excel_rows->pluck('sku'));
        $webike_prices = $this->mappingService->getProductPrices($webike_products->pluck('id'), date('Y-m-d'));
        $statuses = \App\Models\Status::get();
        $eager_load_mappingProducts = \App\Models\MappingProduct::whereIn('id', $excel_rows->pluck('比對結果編號'))->get();

        DB::transaction(function () use ($excel_rows, $statuses, $eager_load_mappingProducts, $webike_products, $webike_prices) {
            foreach ($excel_rows as $excel_row) {
                if ($excel_row['對手商品編號']) {
                    $sku = $excel_row['sku'];
                    $status = $statuses->firstWhere('display_name', $excel_row['商品狀態']);
                    if ($status === null) dd("狀態錯誤:'" . $excel_row['商品狀態'] . "'");

                    if ($excel_row['比對結果編號']) {  //更新mappingProduct
                        $mappingProduct = $eager_load_mappingProducts->firstWhere('id', $excel_row['比對結果編號']);
                    } else {//新增mappingProduct
                        $mappingProduct = new \App\Models\MappingProduct;
                        $mappingProduct->outer_item_id = $excel_row['對手商品編號'];
                        $mappingProduct->sku = null;
                    }

                    $mappingProduct->status_id = $status->id;
                    $webike_product = $webike_products->firstWhere('sku', $sku);
                    $webike_price_c = $webike_prices->where('product_id', $webike_product->id ?? null)->firstWhere('role_id', 2);
                    $webike_price_d = $webike_prices->where('product_id', $webike_product->id ?? null)->firstWhere('role_id', 3);

                    $mappingProduct->product_id = $webike_product->id ?? null;
                    $mappingProduct->webike_name = $webike_product->name ?? null;
                    $mappingProduct->model_number = $webike_product->model_number ?? null;
                    $mappingProduct->webike_price_c = $webike_price_c->price ?? null;
                    $mappingProduct->webike_price_d = $webike_price_d->price ?? null;
                    if ($mappingProduct->sku != $sku && in_array($mappingProduct->status_id, [Status::PRICE_LEVEL_2, Status::PRICE_LEVEL_1, Status::PRICE_LEVEL_0])) {
                        $mappingProduct->status_id = Status::WAITING_UPDATE;
                        $mappingProduct->apply_price = 0;
                    }
                    //if sku changed in updated, force to update
                    $mappingProduct->sku = $sku;
                    $mappingProduct->save();

                }
            }
        });
        echo collect(request()->file())->first()->getClientOriginalName() . " 更新完成(" . date("Y-m-d H:i:s") . ")<br>";
    }




    public function getBrandList()
    {
        $brand_ids = \App\Models\Seller::select('manufacturer_id')->groupBy('manufacturer_id')->get()->pluck('manufacturer_id');
//        dd($brand_ids);

        $brand_ids_string = '';
        foreach ($brand_ids as $brand_id) {
            $brand_ids_string .= $brand_id . ',';
        }
        try {
//                throw new \Exception('test');
            $brand_details = file_get_contents(config('api.brand_query') . "?brand_ids=" . $brand_ids_string);
            $brand_details = json_decode($brand_details);
            $brand_details = collect($brand_details);
        } catch (\Exception $e) {
            $brand_details = collect([]);
            $message = "brand query api get some problem, plz contact Frank :{{ Error: " . $e->getMessage();
            \Log::critical($message);
        }

        $brands_info = DB::connection('eg_sniper')->select("SELECT
	s.manufacturer_id AS brand_id,
	sum(
	CASE
			
			WHEN mp.status_id = 8 || mp.status_id = 9 || mp.status_id = 10 || mp.status_id = 11 THEN
			1 ELSE 0 
		END 
		) AS '與競爭對手比對的商品數',
		sum( CASE WHEN mp.apply_price = 1 THEN 1 ELSE 0 END ) AS '_1級設定',
		sum( CASE WHEN mp.status_id = 13 THEN 1 ELSE 0 END ) AS '待確認',
		sum( CASE WHEN mp.status_id = 9 THEN 1 ELSE 0 END ) AS '有競爭力',
		sum( CASE WHEN mp.status_id = 10 THEN 1 ELSE 0 END ) AS '勉強有競爭力',
		sum( CASE WHEN mp.status_id = 11 THEN 1 ELSE 0 END ) AS '無競爭力',
		( SELECT count( * ) FROM sellers WHERE manufacturer_id = s.manufacturer_id ) AS '賣家數',
		( SELECT count( * ) FROM sellers WHERE status_id = 4 && manufacturer_id = s.manufacturer_id ) AS '未抓賣家數' 
	FROM
		sellers AS s
		LEFT JOIN outer_items AS oi ON s.id = oi.seller_id
		LEFT JOIN mapping_products AS mp ON oi.id = mp.outer_item_id 
	GROUP BY
		s.manufacturer_id 
	ORDER BY
		sum(
		CASE
				
				WHEN mp.status_id = 8 || mp.status_id = 9 || mp.status_id = 10 || mp.status_id = 11 THEN
				1 ELSE 0 
		END 
	) DESC");
        $brands_info = collect($brands_info);


//            try {
//                $api = config('api.account_receives') . "?brand_ids=$brand_ids_string";
//                $task = Task::firstOrNew(['title' => 'receive_api']); //this is for debug purpose
//                $task->message = $api;
//                $task->save();
//
//                $brand_receives = file_get_contents($api);
//                $brand_receives = json_decode($brand_receives);
//            } catch (\Exception $e) {
//                $message = '售注api錯誤 error:' . $e->getMessage();
//                \Log::error($message);
//                $brand_receives = null;
//            }
        $brand_receives = null;

        $brands = [];
        foreach ($brand_ids as $brand_id) {
            $brand = new \stdClass();
            $brand->id = $brand_id;
            $brand->name = $brand_id;

            $brand_detail = $brand_details->firstWhere('id', $brand_id);
            $brand_info = $brands_info->firstWhere('brand_id', $brand_id);
            if ($brand_detail !== null) $brand->name = $brand_detail->title;

            $brand->與競爭對手比對的商品數 = $brand_info->與競爭對手比對的商品數;
            $brand->_1級設定 = $brand_info->_1級設定;
            $brand->待確認 = $brand_info->待確認;
            $brand->有競爭力 = $brand_info->有競爭力;
            $brand->勉強有競爭力 = $brand_info->勉強有競爭力;
            $brand->無競爭力 = $brand_info->無競爭力;
            $brand->賣家數 = $brand_info->賣家數;
            $brand->未抓賣家數 = $brand_info->未抓賣家數;
            $brand->receives = $brand_receives->$brand_id ?? null;// 品牌售注

            $brands[] = $brand;
        }
        $year = date('Y');
        $brands = collect($brands)->sortByDesc("receives.$year");


        $cell_data = [
            [
                '品牌',
                '與競爭對手比對的商品數',
                '_1級設定',
                '待確認 對手剩餘商品數',
                '有競爭力',
                '勉強有競爭力',
                '無競爭力',
                '最後處理人',

            ]
        ];
        foreach ($brands as $brand) {
            $cell_data[] = [
                $brand->name,
                $brand->與競爭對手比對的商品數,
                $brand->_1級設定,
                $brand->待確認,
                $brand->有競爭力,
                $brand->勉強有競爭力,
                $brand->無競爭力,
                '--',


            ];
        }
        if (request()->has('download')) {
            ExcelService::export('manage', 'manage', $cell_data);
        }

        return view('brand_list', compact('brands'));
    }

    public function postBrandList()
    {

        $brand_ids = request()->get('options') ?? [];
        $sellers = \App\Models\Seller::whereIn('manufacturer_id', $brand_ids)->delete();
        return redirect()->route('get_brand_list');

    }


    public function brand_panel($brand_id)
    {

        $brand_info = DB::connection('eg_sniper')->select("SELECT
	sum( CASE WHEN mp.`status_id` = 9 && apply_price = 0 THEN 1 ELSE 0 END ) AS level_2_no_apply,
	sum( CASE WHEN mp.`status_id` = 9 && apply_price = 1 THEN 1 ELSE 0 END ) AS level_2_apply,
	sum( CASE WHEN mp.`status_id` = 10 && apply_price = 0 THEN 1 ELSE 0 END ) AS level_1_no_apply,
	sum( CASE WHEN mp.`status_id` = 10 && apply_price = 1 THEN 1 ELSE 0 END ) AS level_1_apply,
	sum( CASE WHEN mp.`status_id` = 11 && apply_price = 0 THEN 1 ELSE 0 END ) AS level_0_no_apply,
	sum( CASE WHEN mp.`status_id` = 11 && apply_price = 1 THEN 1 ELSE 0 END ) AS level_0_apply,
	sum( CASE WHEN mp.`status_id` = 8 = 1 THEN 1 ELSE 0 END ) AS waiting_update,
	sum( CASE WHEN mp.`status_id` = 13 THEN 1 ELSE 0 END ) AS non_check,
	sum( CASE WHEN mp.`status_id` = 12 THEN 1 ELSE 0 END ) AS non_correspond,
	( SELECT count( * ) FROM sellers WHERE sellers.manufacturer_id = oi.manufacturer_id ) AS num_of_seller,
	( SELECT count( * ) FROM sellers WHERE sellers.manufacturer_id = oi.manufacturer_id && sellers.status_id = 4 ) AS non_finish_seller
	
FROM
	outer_items AS oi
	JOIN mapping_products AS mp ON oi.id = mp.outer_item_id 
WHERE
	oi.manufacturer_id = $brand_id");

        $brand_info = $brand_info[0];
//        dd($brand_info[0]);
//        {#744 ▼
//            +"has_compe_non_apply": "0"
//        +"has_compe_apply": "716"
//        +"half_compe_non_apply": "0"
//        +"half_compe_apply": "8"
//        +"non_compe_non_apply": "51"
//        +"non_compe_apply": "0"
//        +"non_check": "5"
//        +"non_correspond": "1"
//        +"created_at": "2019-01-10 17:00:48"
//}


        $mapping_prodcuts = \App\Models\MappingProduct::whereHas('outerItem', function ($query) use ($brand_id) {
            $query->where('manufacturer_id', $brand_id);
        })->get();
        try {
//            $webike_products = DB::connection('eg_product')->table('products')
//                ->where('manufacturer_id', $brand_id)
//                ->where('is_main', 1)
//                ->where('active', 1)
//                ->where('type', 0)
//                ->get();
//            $webike_products = $webike_products->whereNotIn('sku', $mapping_prodcuts->pluck('sku'));
            $num_of_webike_mapping_fail = $webike_products->count();
        } catch (\Exception $e) {
            $num_of_webike_mapping_fail = '--';
        }


        return view('brand_panel', compact(
            'brand_id',
            'num_of_webike_mapping_fail',
            'brand_info'
        ));
    }

    public function mapping_result()
    {
        $brand_id = request()->get('brand_id') ?? 'fuck';
        $status_id = request()->get('status_id') ?? 'fuck';

        $mapping_products = MappingProduct::with('outerItem', 'status')
            ->where('status_id', $status_id)
            ->whereHas('outerItem', function ($query) use ($brand_id) {
                $query->where('manufacturer_id', $brand_id);
            })
            ->get();

        return view('mapping_result', compact('mapping_products', 'brand_id'));

    }


    public function mapping_fail(Request $request, $brand_id)
    {

        $mapping_prodcuts = \App\Models\MappingProduct::whereHas('outerItem', function ($query) use ($brand_id) {
            $query->where('manufacturer_id', $brand_id);
        })->get();
        $webike_products = DB::connection('eg_product')->table('products')
            ->where('manufacturer_id', $brand_id)
            ->where('is_main', 1)
            ->where('active', 1)
            ->where('type', 0)
            ->get();
        $webike_products = $webike_products->whereNotIn('sku', $mapping_prodcuts->pluck('sku')->all());

        return view('item', compact('webike_products', 'brand_id'));
    }

    function httpPost($url, $data)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }



    public function getEditBrand(){
        if (request()->has('brand_id')) {
            $brand_id = request()->get('brand_id') ?? false;
            if (!$brand_id) throw new \Exception('WTF');

            $brand = new \stdClass();
            $brand->id = $brand_id;
            try {
                $brand_query = file_get_contents(config('api.brand_query') . '?brand_ids=' . $brand_id);
                $brand_query = json_decode($brand_query, true);

                $brand->name = $brand_query[0]['title'];
            } catch (\Exception $e) {
                $message = "Brand query api get some problem, plz contact Frank. Error: " . $e->getMessage();
                \Log::error($message);

                $brand->name = $brand_id;
            }

            $sellers = \App\Models\Seller::with('items')->where('manufacturer_id', $brand_id)->get();

            return view('brand_edit', compact('brand', 'sellers'));
        }

        return view('brand_edit');
    }

    public function postEditBrand(){
        $brand_id = request()->get('brand_id');
        $seller_names = request()->get('seller_names') ?? [];
        $seller_urls = request()->get('seller_urls') ?? [];
        $mode = request()->get('mode') ?? '';
        if (count($seller_names) != count($seller_urls)) throw new \Exception("seller's name and url's quantity not equal, plz contact Frank");

        DB::transaction(function () use ($seller_urls, $brand_id, $seller_names, $mode) {
            foreach ($seller_urls as $key => &$seller_url) {
                if ($seller_url == null) {
                    $seller_url = '';
                    continue;
                }
                $seller = \App\Models\Seller::firstOrNew([
                    'manufacturer_id' => $brand_id,
                    'url' => $seller_url,
                ]);
                $seller->name = $seller_names[$key];
                if ($seller->status_id === null) $seller->status_id = Status::WAITING_CRAWL;
                $seller->save();
            }
            unset($seller_url);

            if ($mode == 'edit') {
                \App\Models\Seller::where('manufacturer_id', $brand_id)
                    ->whereNotIn('url', $seller_urls)->delete();
            }

        });

        return redirect()->route('get_brand_list');
    }


    public function getEditMapping()
    {
        $brand_id = \request()->get('brand_id');
        $status_id = \request()->get('status_id');

        $mapping_products = \App\Models\MappingProduct::with('outerItem', 'status')
            ->whereHas('outerItem', function ($query) use ($brand_id) {
                $query->where('manufacturer_id', $brand_id);
            })
            ->where('status_id', $status_id)
            ->paginate(100);


        return view('mapping_edit', compact('brand_id', 'mapping_products', 'status_id'));
    }

    public function postEditMapping()
    {
        $brand_id = request()->get('brand_id') ?? -1;
        $skus = request()->get('skus') ?? [];
        $statuses = request()->get('statuses') ?? [];
        $webike_products = $this->mappingService->getProductBySkus(array_values($skus));
        $webike_prices = $this->mappingService->getProductPrices($webike_products->pluck('id'), date('Y-m-d'));
        foreach ($skus as $mapping_product_id => $sku) {
            $mappingProduct = \App\Models\MappingProduct::where('id', $mapping_product_id)->first();
            $mappingProduct->status_id = $statuses[$mapping_product_id];

            $webike_product = $webike_products->firstWhere('sku', $sku);
            $webike_price_c = $webike_prices->where('product_id', $webike_product->id ?? null)->firstWhere('role_id', 2);
            $webike_price_d = $webike_prices->where('product_id', $webike_product->id ?? null)->firstWhere('role_id', 3);

            $mappingProduct->product_id = $webike_product->id ?? null;
            $mappingProduct->webike_name = $webike_product->name ?? null;
            $mappingProduct->model_number = $webike_product->model_number ?? null;
            $mappingProduct->webike_price_c = $webike_price_c->price ?? null;
            $mappingProduct->webike_price_d = $webike_price_d->price ?? null;
            if ($mappingProduct->sku != $sku && in_array($mappingProduct->status_id, [Status::PRICE_LEVEL_0, Status::PRICE_LEVEL_1, Status::PRICE_LEVEL_2])) {
                $mappingProduct->status_id = Status::WAITING_UPDATE;
                $mappingProduct->apply_price = 0;
            }
            //if sku changed in updated, force to update
            $mappingProduct->sku = $sku;
            $mappingProduct->save();
        }
//            return redirect()->route('brand_panel', ['id' => $brand_id]);
        return back();
    }


}







