<?php

namespace App\Http\Controllers;

use App\Models\Test\Brand;
use Illuminate\Http\Request;

class APIController extends Controller
{
    public function queryBrand(){

        if (\request()->has('keyword')) {
            $keyword = \request()->get('keyword');
            $brands = Brand::where('title', 'like', "%$keyword%")->get();
        }

        if (\request()->has('brand_ids')) {
            $brand_ids = explode(',', \request()->get('brand_ids'));
            $brands = Brand::whereIn('id', $brand_ids)->get();
        }
        if (!isset($brands)) $brands='keyword or brand_ids ?';

        return response()->json($brands);
    }
}
