<?php

namespace App\Console\Commands;

use App\Models\Log\Task;
use App\Service\Status;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use App\Service\MappingService;

class ApplyPrice extends Command
{

    protected $mappingService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = \App\Constants\Command::APPLY_PRICE;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'apply price';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->mappingService = new MappingService;
//        $this->mappingService = $mappingService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

//        需應用價格商品
        $mapping_products = \App\Models\MappingProduct::whereIn('status_id', [Status::PRICE_LEVEL_2, Status::PRICE_LEVEL_1])
//            ->take(20)
            ->get();

        $mapping_products = $mapping_products->groupBy('sku');  //分組SKU
        $mapping_products = $mapping_products->map(function ($sku_group, $key) {   //排序mapping_product 價格小到大，最後只取第1筆應用
            return $sku_group->sortBy('webike_price_c_new');
        });
//dd($mapping_products->count());

        $data = [
            'confirm' => 1,
            'source' => 'Sniper',
            'prices' => [
//                ['product_id' => '766', 'role_id' => '2', 'price' => '1000'],
//                ['product_id' => '767', 'role_id' => '1', 'price' => '1200'],
            ],
        ];
        foreach ($mapping_products as $mapping_product) {
            //只取最小價格做更新，若以更新則跳過
            if ($mapping_product->first()->apply_price == 1) continue;
            $data['prices'][] = [
                'product_id' => $mapping_product->first()->product_id,
                'role_id' => 1,
                'price' => $mapping_product->first()->webike_price_c_new
            ];
            $data['prices'][] = [
                'product_id' => $mapping_product->first()->product_id,
                'role_id' => 2,
                'price' => $mapping_product->first()->webike_price_d_new
            ];

        }
        $num_of_price_update = count($data['prices']);

        echo "\n更新數:" . $num_of_price_update . " 更新請求發送中:" . date('H:i:s') . "\n";
        $api_result = $this->mappingService->httpPost(config('api.apply_price'), $data);
        $api_result_decode = json_decode($api_result);
        if ($api_result_decode->success ?? false) {

            echo "\n價格應用成功," . date('H:i:s') . "\n";

            foreach ($mapping_products as $mapping_product) {
                //只取最小價格做更新，若以更新則跳過
                if ($mapping_product->first()->apply_price == 1) continue;
                DB::transaction(function () use ($mapping_product) {
                    $mapping_product->first()->apply_price = 1;
                    $mapping_product->first()->save();


                });

            }

            $message = "1級更新成功, 更新數: $num_of_price_update";
        } else {
            $message = "1級更新失敗, 更新數: $num_of_price_update, api結果: " . $api_result;

        }

        $task=Task::firstOrNew(['title'=>$this->signature]);
        $task->updated_at=date('Y-m-d H:i:s');
        $task->message=$message;
        $task->save();
        echo ("\n" . $message . "\n");
    }
}
