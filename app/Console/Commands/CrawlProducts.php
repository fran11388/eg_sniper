<?php

namespace App\Console\Commands;

use App\Models\Log\Task;
use App\Service\Crawler\Adapter\SellerProducts\AdapterFactory as SellerProductsAdapterFactory;
use App\Service\Crawler\Adapter\SellerProducts\Product;
use App\Service\Status;
use Illuminate\Console\Command;

use Illuminate\Support\Facades\DB;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;

use App\Service\DriverService;

class CrawlProducts extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = \App\Constants\Command::CRAWL_SELLER;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'crawl products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $sellers = \App\Models\Seller::where('status_id', Status::WAITING_CRAWL)
            ->get();
        $manufacturer_ids = $sellers->pluck('manufacturer_id');
        $manufacturers = $this->queryManufacturers($manufacturer_ids); //eager load manufacturer

        $bar = $this->output->createProgressBar(count($sellers));
        $message=[];
        foreach ($sellers as $seller) {
            $seller_url = $seller->url;
            $manufacturer_id = $seller->manufacturer_id;
            $brandName = $manufacturers->firstWhere('id', $manufacturer_id)->name;

            $sellerProductsAdapter = SellerProductsAdapterFactory::create($seller_url);
            if(!$sellerProductsAdapter) {
                $message[]='Invalid seller url: '.$seller_url;
                continue;
            }
            $products = $sellerProductsAdapter->crawlProducts($seller_url, $brandName);

            $this->updateDB($seller->id, $manufacturer_id, $products);

            \App\Models\Seller::where('id', $seller->id)
                ->update(['status_id' => Status::COMPLETED]);

            $bar->advance();
        }
        $bar->finish();

        $task=Task::firstOrNew(['title'=>$this->signature]);
        $task->updated_at=date('Y-m-d H:i:s');
        $task->message=json_encode($message);
        $task->save();


    }

    public function queryManufacturers($manufacturer_ids)
    {
        try {
            $items = \DB::connection('eg_product')->table('manufacturers')
                ->whereIn('id', $manufacturer_ids)
                ->get();
        } catch (\Exception $e) {
            $message = "eg_product's connection get some problem.";

            $task=Task::firstOrNew(['title'=>$this->signature]);
            $task->updated_at=date('Y-m-d H:i:s');
            $task->message=json_encode($message);
            $task->save();
        }

        return $items;
    }


    /**
     * @param $seller_id
     * @param $manufacturer_id
     * @param Product[] $products
     */
    public function updateDB($seller_id, $manufacturer_id, $products)
    {
        foreach ($products as $product) {
            $name = $product->name;
            $price_min = $product->price_min;
            $price_max = $product->price_max;
            $product_url = $product->url;

            $outer_item = \App\Models\OuterItem::where('url', $product_url)
                ->where('seller_id', $seller_id)
                ->first();
            if ($outer_item != null) {
                if ($outer_item->price_min != $price_min) {// 比對價格有無異動

                    DB::transaction(function () use ($outer_item, $price_min, $price_max) {
                        $ItemChangeLog = new \App\Models\ItemChangeLog;
                        $ItemChangeLog->outer_item_id = $outer_item->id;
                        $ItemChangeLog->price = $outer_item->price_min;
                        $ItemChangeLog->new_price = $price_min;
                        $ItemChangeLog->save();

                        $outer_item->price_min = $price_min;
                        $outer_item->price_max = $price_max;
                        $outer_item->save();

                        if ($outer_item->mappingProducts != null) {
                            foreach ($outer_item->mappingProducts as $mappingProduct) {
                                if (in_array($mappingProduct->status_id, [Status::PRICE_LEVEL_0, Status::PRICE_LEVEL_1, Status::PRICE_LEVEL_2])) {
                                    $mappingProduct->status_id = Status::WAITING_UPDATE;
                                    $mappingProduct->apply_price = 0; //取消價格應用
                                    $mappingProduct->save();
                                }
                            }
                        }

                    });

                }
                $outer_item->name = $name;
                $outer_item->updated_at = date("Y-m-d H:i:s");
                $outer_item->save();

            } else {
                $OuterItem = new \App\Models\OuterItem;
                $OuterItem->seller_id = $seller_id;
                $OuterItem->manufacturer_id = $manufacturer_id;
                $OuterItem->url = $product_url;
                $OuterItem->name = $name;
                $OuterItem->price_min = $price_min;
                $OuterItem->price_max = $price_max;
                $OuterItem->description = '';


                $OuterItem->save();
            }

        }
    }

}
