<?php

namespace App\Console\Commands;

use App\Models\Log\Task;
use App\Service\Status;
use Illuminate\Console\Command;
use App\Service\MappingService;

class CalculatePrice extends Command
{

    protected $mappingService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = \App\Constants\Command::CALCULATE_PRICE;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculate C,D price and newC,newD,TP,FP..and so on';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(MappingService $mappingService)
    {
        parent::__construct();
        $this->mappingService = $mappingService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mappingProducts = \App\Models\MappingProduct::with('outerItem')
            ->where('status_id', Status::WAITING_UPDATE)->get();

//        eager load here
        $mappingProducts_skus = $mappingProducts->pluck('sku');
        $webike_products = $this->mappingService->getProductBySkus($mappingProducts_skus);
        $product_ids = $webike_products->pluck('id');
        $webike_prices = $this->mappingService->getProductPrices($product_ids, date('Y-m-d'));
        $query_costs = $this->mappingService->getCostV2($product_ids);

        $message = [];
        $bar = $this->output->createProgressBar(count($mappingProducts));
        foreach ($mappingProducts as $mappingProduct) {
            $sku = $mappingProduct->sku;
            $webike_product = $webike_products->firstWhere('sku', $sku);
            if ($webike_product == null) {
                $message[] = "檢測到錯誤的SKU: $sku, mapping_product's id: " . $mappingProduct->id;

                $mappingProduct->status_id = Status::NOT_CHECK;
                $mappingProduct->save();
                continue;
            }

            $outer_price = $mappingProduct->outerItem->price_min;

            $product_id = $webike_product->id;
            $webike_name = $webike_product->name;
            $model_number = $webike_product->model_number;
            $webike_cost = $query_costs->firstWhere('product_id', $product_id);

            $webike_item_url = "https://www.webike.tw/sd/$sku";

            $webike_price_c = $webike_prices->where('role_id', 2)->firstWhere('product_id', $product_id);
            $webike_price_d = $webike_prices->where('role_id', 3)->firstWhere('product_id', $product_id);

            $new_price = $this->mappingService->getNewPriceV2($outer_price, $webike_cost, $product_id, $webike_prices);

            $webike_price_c_new_TP = $this->mappingService->get_TP_percentV2($query_costs, $product_id, $new_price['c_price']);
            $webike_price_d_new_TP = $this->mappingService->get_TP_percentV2($query_costs, $product_id, $new_price['d_price']);
            $webike_price_c_new_FP = $this->mappingService->get_FP_percentV2($query_costs, $product_id, $new_price['d_price']);
            $webike_price_d_new_FP = $this->mappingService->get_FP_percentV2($query_costs, $product_id, $new_price['d_price']);

            switch ($new_price['price_level']) {
                case 2:
                    $status_id = Status::PRICE_LEVEL_2;
                    break;
                case 1:
                    $status_id = Status::PRICE_LEVEL_1;
                    break;
                case 0:
                    $status_id = Status::PRICE_LEVEL_0;
                    break;
            }


            $mappingProduct->product_id = $product_id;
            $mappingProduct->webike_name = $webike_name;
            $mappingProduct->model_number = $model_number;
            $mappingProduct->webike_price_c = $webike_price_c->price ?? -1;
            $mappingProduct->webike_price_d = $webike_price_d->price ?? -1;
            $mappingProduct->webike_price_c_new = $new_price['c_price'];
            $mappingProduct->webike_price_d_new = $new_price['d_price'];
            $mappingProduct->webike_min_price = null;
            $mappingProduct->webike_product_total_cost = $webike_cost->total_cost;
            $mappingProduct->webike_product_cost_ntd = $webike_cost->cost_local;
            $mappingProduct->webike_product_cost_jpy = $webike_cost->cost;
            $mappingProduct->c_price_vs_outer_price = null;
            $mappingProduct->webike_item_url = $webike_item_url;
            $mappingProduct->webike_price_c_new_TP = $webike_price_c_new_TP;
            $mappingProduct->webike_price_d_new_TP = $webike_price_d_new_TP;
            $mappingProduct->webike_price_c_new_FP = $webike_price_c_new_FP;
            $mappingProduct->webike_price_d_new_FP = $webike_price_d_new_FP;
            $mappingProduct->new_c_price_sub_outer_price = null;
            $mappingProduct->new_c_price_sub_outer_price_div_new_c = null;
            $mappingProduct->status_id = $status_id;
            if ($mappingProduct->webike_price_c_new !== $new_price['c_price']) $mappingProduct->apply_price = 0;
            if ($mappingProduct->webike_price_d_new !== $new_price['d_price']) $mappingProduct->apply_price = 0;

            $mappingProduct->save();

            $bar->advance();
        }
        $bar->finish();

        $task = Task::firstOrNew(['title' => $this->signature]);
        $task->updated_at = date('Y-m-d H:i:s');
        $task->message=json_encode($message);
        $task->save();

    }
}
