<?php

namespace App\Console\Commands;

use App\Models\Log\Task;
use App\Service\Status;
use Illuminate\Console\Command;
use App\Service\MappingService;
use Illuminate\Support\Facades\DB;

class MappingProducts extends Command
{

    protected $mappingService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = \App\Constants\Command::MAPPING_PRODUCT;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mapping products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(MappingService $mappingService)
    {
        parent::__construct();
        $this->mappingService = $mappingService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $mapped_items_id = \App\Models\MappingProduct::pluck('outer_item_id')->all();

        $items = \App\Models\OuterItem::whereNotIn('id', $mapped_items_id)
//            ->take(500)
//                ->where('manufacturer_id',245)
            ->get();




        $bar = $this->output->createProgressBar(count($items));
        foreach ($items as $key => $item) {
            $outer_name = $item->name;
            $manufacturer_id = $item->manufacturer_id;

//            $concurrent_search_result = $concurrent_search_results->firstWhere('id', $item->id);

            $solr_keyword = $this->mappingService->parseSolrKeyword($outer_name);
            $solr_search_result = $this->mappingService->solrSearch($solr_keyword, $manufacturer_id);

            $MappingProduct = new \App\Models\MappingProduct;
            $MappingProduct->outer_item_id= $item->id;
            $MappingProduct->solr_keyword = $solr_keyword;
            $MappingProduct->status_id = Status::NOT_CHECK;
            if ($solr_search_result->response->numFound > 0) {
                $solr_product = $solr_search_result->response->docs[0];
                $sku = $solr_product->sku;
                $webike_item_url = "https://www.webike.tw/sd/$sku";

                $MappingProduct->product_id = $solr_product->id;
                $MappingProduct->sku = $sku;
                $MappingProduct->webike_name = $solr_product->name;
                $MappingProduct->model_number = $solr_product->model_number;
                $MappingProduct->webike_price_c = $solr_product->price_2;
                $MappingProduct->webike_price_d = $solr_product->price_3;
                $MappingProduct->webike_item_url = $webike_item_url;

            }
            $MappingProduct->save();

            $bar->advance();
        }
        $bar->finish();

        $task=Task::firstOrNew(['title'=>$this->signature]);
        $task->updated_at=date('Y-m-d H:i:s');
        $task->save();

    }
}


//$bar = $this->output->createProgressBar(count($items));
//$bar->advance();
//$bar->finish();