<?php

namespace App\Console\Commands;

use App\Models\Log\Task;
use Illuminate\Console\Command;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Promise;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;

use DB;

class DeleteExpired extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = \App\Constants\Command::DELETE_EXPIRED;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'delete the expired product';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $_2_week_ago = \Carbon\Carbon::now()->subWeek(2)->toDateTimeString();
        $expire_outer_items = \App\Models\OuterItem::where('updated_at', '<', $_2_week_ago)->get();
        $expire_count = $expire_outer_items->count();

        DB::transaction(function () use (&$expire_outer_items) {
            foreach ($expire_outer_items as $expire_outer_item) {
                $expired_item=new \App\Models\ExpiredItem;
                $expired_item->url=$expire_outer_item->url;
                $expired_item->save();

                $expire_outer_item->delete();
            }
        });


        $message="下架商品刪除: ".$expire_count;
        $task = Task::firstOrNew(['title' => $this->signature]);
        $task->updated_at = date('Y-m-d H:i:s');
        $task->message=($message);
        $task->save();
        echo "\n".$message."\n";

    }

    public function getRutenProductId($ruten_item_url)
    {
        $re = '/\?.*/';
//        $str = 'https://goods.ruten.com.tw/item/show?21846992644386';

        preg_match($re, $ruten_item_url, $matches, PREG_OFFSET_CAPTURE, 0);

// Print the entire match result

        return str_replace("?", "", $matches[0][0]);

    }
}
