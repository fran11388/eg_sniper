<?php

namespace App\Console\Commands;

use App\Models\Log\Task;
use App\Models\MappingProduct;
use App\Models\Seller;
use App\Service\Status;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ResetStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = \App\Constants\Command::RESET_STATUS;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Seller::where('status_id', '<>', Status::WAITING_CRAWL)->update(['status_id' => Status::WAITING_CRAWL]);

        MappingProduct::whereIn('status_id', [
            Status::PRICE_LEVEL_0,
            Status::PRICE_LEVEL_1,
            Status::PRICE_LEVEL_2,
        ])->update([
            'status_id' => Status::WAITING_UPDATE,
            'apply_price' => 0
        ]);

        $task = Task::firstOrNew(['title' => $this->signature]);
        $task->updated_at = date('Y-m-d H:i:s');
        $task->save();

    }
}
