<?php

namespace App\Console\Commands;

use App\Models\Log\Task;
use App\Service\MappingService;
use Illuminate\Console\Command;

class SyncPrice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = \App\Constants\Command::SYNC_PRICE;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "ensure the SMAX-DB's price record is correctly";

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $api_result = file_get_contents(config('api.query_price_record') . '?source=Sniper');
        } catch (\Exception $e) {
            $message = 'query price record api get some problem: ' . $e->getMessage();
            $task=Task::firstOrNew(['title'=>$this->signature]);
            $task->updated_at=date('Y-m-d H:i:s');
            $task->message=($message);
            $task->save();
            exit($message);
        }
        $smax_price_records = collect(json_decode($api_result));

        $mapping_products = \App\Models\MappingProduct::where('apply_price', 1)->get();
        $product_ids = $mapping_products->pluck('product_id');

        $expired_price_records = $smax_price_records->whereNotIn('product_id', $product_ids);
//        dd($expired_price_records);
        echo "\nSMAX需刪除價格數: " . $expired_price_records->count() . "\n";

        $data = [
            'confirm' => 1,
            'source' => 'Sniper',
            'prices' => [
//                ['product_id' => '766', 'role_id' => '2', 'price' => '1000'],
//                ['product_id' => '767', 'role_id' => '1', 'price' => '1200'],
            ],
        ];
        foreach ($expired_price_records as $expired_price_record) {
            $data['prices'][] = [
                'product_id' => $expired_price_record->product_id,
                'role_id' => $expired_price_record->role_id,
            ];
        }

//        dd($data);
        $MappingService = new MappingService();

        try {
            $api_result = $MappingService->httpPost(config('api.delete_price'), $data);
        } catch (\Exception $e) {
            $message = "delete price api get some problem: " . $e->getMessage();
            \Log::critical($message);
            exit($message);
        }

        $api_result_decode = json_decode($api_result);
        if ($api_result_decode->success ?? false) {
            $message = "1級刪除價格數: " . $expired_price_records->count();
        } else {
            $message = "1級需刪除價格數: " . $expired_price_records->count() . ", 刪除失敗, api結果: " . $api_result;
        }

        $task=Task::firstOrNew(['title'=>$this->signature]);
        $task->updated_at=date('Y-m-d H:i:s');
        $task->message=($message);
        $task->save();
        echo "\n" . $message . "\n";

    }
}
