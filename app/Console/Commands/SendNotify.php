<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SendNotify extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:notify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send email notify';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $receivers = [
            'frank_yan@everglory.asia',
            'mark_su@everglory.asia',

        ];

        \Mail::to($receivers)->send(new \App\Mail\Notify());
        echo 'mail send successfully';

    }
}
