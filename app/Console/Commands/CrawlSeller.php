<?php

namespace App\Console\Commands;

use App\Service\Crawler\Adapter\SellerProducts\AdapterFactory;
use Illuminate\Console\Command;

use Illuminate\Support\Facades\DB;

class CrawlSeller extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crawl:seller {seller_url}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'crawl the specify seller, this feature temporary only support Ruten, shopee';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $seller_url = $this->argument('seller_url');

        $sellerProductAdapter = AdapterFactory::create($seller_url);
        if (!$sellerProductAdapter) {
            echo 'Invalid seller url: ' . $seller_url;
            return;
        }
        $products = $sellerProductAdapter->crawlProducts($seller_url);

        DB::connection('eg_sniper')->table('temporary_store')
            ->delete();
        foreach ($products as $product) {
            DB::connection('eg_sniper')->table('temporary_store')->insert([
                'url' => $product->url,
                'name' => $product->name,
                'price_min' => $product->price_min,
                'price_max' => $product->price_max,
            ]);
        }
        echo "total " . count($products) . ' items';

    }
}
