<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Psy\Command\Command;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [


    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('reset:status')->dailyAt('21:00');
        $schedule->command('product:crawl')->everyMinute()->withoutOverlapping(60*3);
        $schedule->command('product:map')->everyMinute()->withoutOverlapping();
        $schedule->command('product:calculate')->everyMinute()->withoutOverlapping();
//        $schedule->command('product:crawl')->dailyAt('23:05');
//        $schedule->command('product:map')->dailyAt('01:00');
//        $schedule->command('product:calculate')->dailyAt('02:00');
        $schedule->command('delete:expired')->dailyAt('00:00');
        $schedule->command('apply:price')->dailyAt('03:30');
        $schedule->command('sync:price')->dailyAt('05:00');
        $schedule->command('send:notify')->dailyAt('09:00');

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
