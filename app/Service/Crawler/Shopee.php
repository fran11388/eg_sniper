<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/29
 * Time: 下午 04:27
 */

namespace App\Service\Crawler;

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;
use GuzzleHttp\Client;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Request;

class Shopee
{


    public function __construct()
    {

    }

    public function getSellerProducts($seller_url, $key_word = null)
    {
        $shop_id = $this->getShopId($seller_url);

        $start = 0;
        $offset = 50;
        $items = [];
        while (true) {
            $client = new Client();

            try {
                $api="https://shopee.tw/api/v2/search_items/?by=relevancy&keyword=$key_word&limit=$offset&match_id=$shop_id&newest=$start&order=desc&page_type=shop";
                $response = $client->request('get', $api, [
                    'headers' => [
//                'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.109 Safari/537.36',
//                'if-none-match-'=>'55b03-6e8ac8f8a62511a7fbb3a9c8641ab074',
//                'x-api-source'=>'pc'
                    ],
                    'form_params' => [

                    ]
                ]);
            } catch (\Exception $e) {
                $message = "shopee api get some problem, error: " . $e->getMessage().", api:$api";
                \Log::critical($message);
                return [];
            }
            $json = json_decode($response->getBody());
            if (count($json->items) == 0) break;
            foreach ($json->items as $item) {
                $items[] = $item;
            }
            $start += $offset;
        }

        if (count($items) == 0) return [];
        $item_id_array = [];
        foreach ($items as $item) {
            $item_id_array[] = $item->itemid;
        }
        $products = $this->getProductDetails($items[0]->shopid, $item_id_array);
        return $products;
    }

    private function getShopId($seller_url) //ex: https://shopee.tw/shop/3143184/search
    {
        try {
            $tokens = explode('/', $seller_url);
            return $tokens[4];
        } catch (\Exception $e) {
            \Log::error("shopee shop id parse error: ".$e->getMessage() . " url: $seller_url");
            return null;
        }

    }

    public function getProductDetails($shop_id, $item_id_array)
    {
        $products = [];

        $requests = function () use ($shop_id, $item_id_array) {
            foreach ($item_id_array as $item_id) {
                $uri = "https://shopee.tw/api/v2/item/get?itemid=$item_id&shopid=$shop_id";
                yield new Request('GET', $uri);
            }
        };

        try {
            $client = new Client();
            $pool = new Pool($client, $requests(), [
                'concurrency' => 10,
                'fulfilled' => function ($response, $index) use (&$products) {
                    // this is delivered each successful response

                    $products[] = json_decode($response->getBody())->item;
                },
                'rejected' => function ($reason, $index) {
                    // this is delivered each failed request
                    throw new \Exception($reason);
                },
            ]);
            $promise = $pool->promise();
            $promise->wait();
        } catch (\Exception $e) {
            $message = "shopee item detail api get some problem, error: " . $e->getMessage();
            \Log::critical($message);
            return [];
        }

        return $products;
    }


}