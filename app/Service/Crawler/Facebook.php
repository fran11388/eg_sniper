<?php
/**
 * Created by PhpStorm.
 * User: User2
 * Date: 2018/2/21
 * Time: 上午 10:06
 */

namespace App\Service\Crawler;

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;

use App\Service\DriverService;

//this file is deprecated
class Facebook
{
    protected $driverService;

//    protected $sellerUrlRegexp ='/.*=/';
//    protected $start_page = 1;
    protected $item_list_script = [
        '
            a=document.querySelectorAll("[id*=\'content_container\'] td > div > div > div > a");
            result=[];
            for(i=0;i<a.length;i++){
                result.push(a[i].href);
            }
            return result;
        ',

    ];

    protected $item_name_list = [
        '
            divs=document.querySelectorAll("[id*=\'content_container\'] td > div > div > div strong ");
            result=[];
            for(i=0;i<divs.length;i++){
                result.push(divs[i].innerText);
            
            }
            return result;    
        ',

    ];

    protected $item_price_list = [
        '
             divs=document.querySelectorAll("[id*=\'content_container\'] td > div > div > div >div ");
             result=[];
             for(i=0;i<divs.length;i++){
                 if(divs[i].querySelector(\'span\')==null){
                     result.push(divs[i].innerText);
                 }               
                 else{
                     result.push(divs[i].querySelector(\'span\').innerText);
                 }
             }
             
             for(i=0;i<result.length;i++){
                
                regex =/\$([0-9,]*)/g;
                reg_result=regex.exec(result[i]);
                str=reg_result[0];
                
                str=str.replace(/NT/g, "");  
                str=str.replace(/\$/g, ""); 
                str=str.replace(/-/g, "");        
                str=str.replace(/,/g, "");      
                str=str.replace(/ /g, ""); 
                result[i]=str;
             }
             
             return result;
        ',
    ];

    protected $name_script = [
        '
            n=document.querySelector("#content > div > div > div > span");
            result=n.innerText;
            return result;
        ',

    ];

    protected $price_script = [
        '
            p=document.querySelector("[class*=\'uiScrollableAreaContent\'] > div > div > div > div > div > div > div> div");

            regex =/\$([0-9,]*)/g;
            reg_result=regex.exec(p.innerText);
            str=reg_result[0];
            
            str=str.replace(/NT/g, "");  
            str=str.replace(/\$/g, ""); 
            str=str.replace(/-/g, "");        
            str=str.replace(/,/g, "");      
            str=str.replace(/ /g, "");  
            
            result=str;
            return result;
        ',

    ];

    protected $description_script = [
        '
            d=document.querySelector("[class*=\'uiScrollableAreaContent\'] > div > div > ul > li:nth-child(1) > div:nth-child(2)");
            result=d.outerHTML;
            return result;
        ',


    ];

    protected $description_showmore_script = [

        '
            sh=document.querySelector("[class*=\'uiScrollableAreaContent\'] > div > div > ul > li > div:nth-child(3) > div:nth-child(1) > a");
            sh.click();
        ',
    ];

    protected $image_script = [
        '
            //url("http....")   => http....
            function bgImageToUrl(str){
                regex = /http.*[^")]/g;
                let m;
                output="";
            
                while ((m = regex.exec(str)) !== null) {
                    // This is necessary to avoid infinite loops with zero-width matches
                    if (m.index === regex.lastIndex) {
                        regex.lastIndex++;
                    }
            
                    // The result can be accessed through the `m`-variable.
                    m.forEach((match, groupIndex) => {
                     //   console.log(`Found match, group ${groupIndex}: ${match}`);
                        output+=match;
                    });
                }
            
                return output;
            }
        
            div=document.querySelectorAll("#content > div > div > div:nth-child(2) > div > div > div:nth-child(1) > div > div > div:nth-child(2) > div");
            
            result=[];
            for(i=0;i<div.length;i++){
            	result.push(bgImageToUrl(div[i].style.backgroundImage));
            }
            return result;
        ',
    ];

    protected $isListScroll = false;
    protected $isItemScroll = false;
    protected $delay_second = 2;

    public function __construct(DriverService $driverService)
    {

        $this->driverService = $driverService;
    }

    public function get_the_page_all_items_name_price_url(RemoteWebDriver $driver, $url)
    {
        $result = [];
        try {
            $driver->get($url);
        } catch (\Exception $e) {
            $message = "load website:$url get some problem:" . $e->getMessage();
            \Log::error($message);
            return $result;
        }

        $names = $this->driverService->driverExecuteScript($driver, $this->item_name_list);
        $prices = $this->driverService->driverExecuteScript($driver, $this->item_price_list);
        $urls = $this->driverService->driverExecuteScript($driver, $this->item_list_script);

        if (count($names) != count($prices) || count($names) != count($urls)) {
            $message = "facebook's crawler script get some problem, names,prices,urls not equal";
            \Log::error($message);
            return $result;
        }

        for ($i = 0; $i < count($names); $i++) {
            $result[] = [
                'name' => $names[$i],
                'price' => $prices[$i],
                'url' => $urls[$i],
            ];
        }

        return $result;
    }


    public function getList(RemoteWebDriver $driver, $url)
    {

        try {
            $driver->get($url);
        } catch (\Exception $e) {
            $message = "load website get some problem:" . $e->getMessage();
            \Log::error($message);
            exit($message);

        }


        if ($this->isListScroll) {
            $this->driverService->driverSimulateScroll($driver, $this->delay_second);
        }

        $item_url_list = $this->driverService->driverExecuteScript($driver, $this->item_list_script);


        return $item_url_list;


    }

    public function getItem(RemoteWebDriver $driver, $url)
    {

        try {
            $driver->get($url);
        } catch (\Exception $e) {
            $message = "load website get some problem:" . $e->getMessage();
            \Log::error($message);
            exit($message);

        }


        if ($this->isItemScroll) {
            $this->driverService->driverSimulateScroll($driver, $this->delay_second);
        }

        $this->driverService->driverExecuteScript($driver, $this->description_showmore_script);  /*simulate click show more link*/

        $name = $this->driverService->driverExecuteScript($driver, $this->name_script);
        $price = $this->driverService->driverExecuteScript($driver, $this->price_script);
        $description = $this->driverService->driverExecuteScript($driver, $this->description_script);
        $image = $this->driverService->driverExecuteScript($driver, $this->image_script);

        $isMissing = false;
        if ($name == null) {
            $isMissing = true;
        }

        return [
            'url' => $url,
            'name' => $name,
            'price' => $price,
            'description' => $description,
            'image' => $image,
            'isMissing' => $isMissing,
        ];

    }


}