<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018/10/3
 * Time: 下午 01:18
 */

namespace App\Service\Crawler;


//Ruten api
//get seller_id  https://rtapi.ruten.com.tw/api/seller/v1/index.php/seller?nick=coony123188
//get seller_item_id  https://rtapi.ruten.com.tw/api/search/v2/index.php/core/seller/7200273/prod?offset=1&limit=50&sort=new%2Fdc&q=agv
//get item_detail  https://rtapi.ruten.com.tw/api/prod/v2/index.php/prod?id=21802594348680,21752509900404,21750442799707,21750411622832,21746344170223,21746344166127,21746344160693,21746344154876,21746344152447,21746339851885,21746335026492,21746334780904,21745320154958,21745320152260,21745320150190,21745320149042,21745320145352,21745320142360,21745319652960,21743261717269,21738122923774,21737103107530,21737103104942,21737103101387,21737103098090,21737103094029,21737103091341,21737103087447,21737103084208,21737103079437

class Ruten
{
    public function getSellerProducts($seller_url, $key_word = null)
    {
        try {
            $nick_name = $this->sellerUrlToNickName($seller_url);
            $seller_id = $this->getSellerId($nick_name);
            $products_id = $this->getSellerProductIds($seller_id, $key_word);
            $product_details = $this->getProductDetail($products_id);
        } catch (\Exception $e) {
            $message = "Ruten api get some problems, plz contact Frank";
            \Log::critical($message);
            return [];
        }

        return $product_details;
    }

    private function sellerUrlToNickName($url)
    {

        $parse_url = parse_url($url, PHP_URL_QUERY);
        parse_str($parse_url, $paramaters);
        $seller_nick_name = $paramaters['s'];
        return $seller_nick_name;
    }

    private function getProductDetail($productsID_array)
    {
        $product_details = [];

        $products_id_chunk = array_chunk($productsID_array, 50);
        foreach ($products_id_chunk as $products_id) {
            $id_string = '';
            foreach ($products_id as $key => $product_id) {
                $id_string .= $product_id;
                if (count($products_id) != $key + 1) {
                    $id_string .= ',';
                }
            }
            $api_result = file_get_contents("https://rtapi.ruten.com.tw/api/prod/v2/index.php/prod?id=$id_string");
            $json = json_decode($api_result, true);

            foreach ($json as $product_detail) {
                $product_details[] = $product_detail;
            }

        }

        return $product_details;

    }

    private function getSellerProductIds($seller_id, $keyword = null)
    {
        $offset = 1;
        $products_id = [];
        $json = [];

        do {
            if (array_key_exists("Rows", $json)) {
                foreach ($json['Rows'] as $item) {
                    $products_id[] = $item['Id'];
                }

            }

            $offset = count($products_id) + 1;
            switch ($keyword) {
                case null:
                    $api_result = file_get_contents("https://rtapi.ruten.com.tw/api/search/v2/index.php/core/seller/$seller_id/prod?offset=$offset&limit=50&sort=new%2Fdc");
                    break;

                default:
                    $keyword = urlencode($keyword);
                    $api_result = file_get_contents("https://rtapi.ruten.com.tw/api/search/v2/index.php/core/seller/$seller_id/prod?offset=$offset&limit=50&sort=new%2Fdc&q=$keyword");
            }
            $json = json_decode($api_result, true);
        } while (count($json['Rows']) != 0);


        return $products_id;

    }

    private function getSellerId($nick_name)
    {
        $api_result = file_get_contents("https://rtapi.ruten.com.tw/api/seller/v1/index.php/seller?nick=$nick_name");
        $json = json_decode($api_result, true);
        return $json[0]['Id'];

    }
}