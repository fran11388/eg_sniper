<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/4/1
 * Time: 下午 02:08
 */

namespace App\Service\Crawler\Adapter\SellerProducts;


use Facebook\WebDriver\Remote\RemoteWebDriver;
use App\Service\Crawler\Shopee as BasicShopee;

class Shopee implements AdapterInterface
{
    protected $crawler;

    public function __construct(BasicShopee $crawler)
    {
        $this->crawler = $crawler;
    }

    /**
     * @param $seller_url
     * @param null $keyword
     * @param RemoteWebDriver|null $driver
     * @return Product[]
     */
    public function crawlProducts($seller_url, $keyword = null, RemoteWebDriver $driver = null): iterable
    {
        // TODO: Implement crawlProducts() method.
        $products = $this->crawler->getSellerProducts($seller_url, $keyword);
        $result = [];
        foreach ($products as $product) {
            $product_obj = new Product();
            $product_obj->url = '//shopee.tw/' . '--i.' . $product->shopid . '.' . $product->itemid;
            $product_obj->price_min = ((int)$product->price_min) / 100000;
            $product_obj->price_max = ((int)$product->price_max) / 100000;
            $product_obj->name = $product->name;
            $product_obj->description = $product->description;

            $result[] = $product_obj;
        }
        return $result;

    }

    public static function create()
    {
        return new static(new BasicShopee());
    }
}