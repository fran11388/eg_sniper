<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/28
 * Time: 下午 05:05
 */

namespace App\Service\Crawler\Adapter\SellerProducts;


use Facebook\WebDriver\Remote\RemoteWebDriver;
use App\Service\Crawler\Ruten as BasicRuten;

class Ruten implements AdapterInterface
{
    protected $crawler;

    public function __construct(BasicRuten $crawler)
    {
        $this->crawler = $crawler;
    }


    /**
     * @param $seller_url
     * @param null $keyword
     * @param RemoteWebDriver|null $driver
     * @return Product[]
     */
    public function crawlProducts($seller_url, $keyword = null, RemoteWebDriver $driver = null): iterable
    {
        // TODO: Implement crawlProducts() method.

        $products = $this->crawler->getSellerProducts($seller_url, $keyword);
        $result = [];
        foreach ($products as $product) {
            $product_obj = new Product();

            $product_obj->name = $product['ProdName'];
            $product_obj->price_min = $product['PriceRange'][0];
            $product_obj->price_max = $product['PriceRange'][1];
            $Image = $product['Image'];
            $product_obj->images[] = "https://a.rimg.com.tw$Image";
            $ProdId = $product['ProdId'];
            $product_obj->url = "https://goods.ruten.com.tw/item/show?$ProdId";

            $result[] = $product_obj;
        }

        return $result;
    }

    public static function create()
    {
        return new static(new BasicRuten());
    }
}