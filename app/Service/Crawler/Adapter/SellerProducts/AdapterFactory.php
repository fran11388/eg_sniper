<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/28
 * Time: 下午 05:21
 */

namespace App\Service\Crawler\Adapter\SellerProducts;


class AdapterFactory
{

    /**
     * @param $seller_url
     * @return AdapterInterface | null
     */
    public static function create($seller_url)
    {
        switch (true) {
            case self::stringContain($seller_url, 'ruten'):
                if (!self::stringContain($seller_url, '?s=')) return null;
                return Ruten::create();
                break;

            case self::stringContain($seller_url, 'shopee'):
                if (!self::stringContain($seller_url, '/shop/')) return null;
                return Shopee::create();
                break;

            default:
                return null;
        }
    }

    public static function stringContain($string, $token)
    {
        return strpos($string, $token) !== false;
    }
}