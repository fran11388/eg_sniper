<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/28
 * Time: 下午 05:06
 */

namespace App\Service\Crawler\Adapter\SellerProducts;
use Facebook\WebDriver\Remote\RemoteWebDriver;

interface AdapterInterface
{
    /**
     * @param $seller_url
     * @param $keyword
     * @param RemoteWebDriver $driver
     * @return Product[]
     */
    public function crawlProducts($seller_url, $keyword=null, RemoteWebDriver $driver=null): iterable;
}