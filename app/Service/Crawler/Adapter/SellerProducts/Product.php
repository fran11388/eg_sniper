<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018/11/27
 * Time: 上午 11:14
 */

namespace App\Service\Crawler\Adapter\SellerProducts;


class Product
{
    public $url;
    public $name;
    public $price_min;
    public $price_max;
    public $description;
    public $images=[];


}