<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018/10/4
 * Time: 下午 04:20
 */

namespace App\Service;

use function Couchbase\defaultDecoder;
use Illuminate\Support\Facades\DB;

class MappingService
{

    public function get_TP_percentV2($query_costs, $product_id, $new_price)
    {
        $webike_cost = $query_costs->firstWhere('product_id', $product_id);
        if ($webike_cost === null) {
            $message = "無法查到product_id: $product_id 的成本";
            \Log::critical($message);
            return 0;
        }
        $ntd_cost = $webike_cost->cost_local;
        $operation_fee = $webike_cost->operation_fee;

        $TP = number_format((($new_price - $ntd_cost - $operation_fee) / $new_price) * 100, 2, '.', '');
        return $TP;
    }


    public function get_FP_percentV2($query_costs, $product_id, $new_price)
    {
        $webike_cost = $query_costs->firstWhere('product_id', $product_id);
        if ($webike_cost === null) {
            $message = "無法查到product_id: $product_id 的成本";
            \Log::critical($message);
            return 0;
        }
        $ntd_cost = $webike_cost->cost_local;


        $FP = number_format((($new_price - $ntd_cost) / $new_price) * 100, 2, '.', '');
        return $FP;
    }


    public function parseSolrKeyword($outer_name)
    {
        $outer_name = $this->removeChinese($outer_name);
        $outer_name = $this->removeTrashString($outer_name);
        $outer_name = $this->removeSolrProblemString($outer_name);
        return $outer_name;
    }


    public function getProductPrices($product_ids, $rule_date)
    {
        try {
            $prices = DB::connection('eg_product')->table('product_prices')
                ->whereIn('product_id', $product_ids)
                ->where('rule_date', $rule_date)
                ->get();
        } catch (\Exception $e) {
            $message = "eg_product's connection get some problem, when query C,D price, plz contact Frank";
            \Log::critical($message);
            exit($message);
        }
        return $prices;

    }


    public function getCostV2($product_ids)
    {
        $product_ids_string = '';
        foreach ($product_ids as $product_id) {
            $product_ids_string .= $product_id . ',';
        }
        $data = [
            'product_ids' => $product_ids_string
        ];

        try {
            $api_result = $this->httpPost(config('api.query_cost'), $data);
            $api_result = json_decode($api_result);
            if(count($product_ids)!==count($api_result->data->products ?? [])){
                $api_path=config('api.query_cost').'?product_ids='.$product_ids_string;
                throw new \Exception('查詢商品數與回傳結果數不同, api: '.$api_path);
            }
            return collect($api_result->data->products ?? []);
        } catch (\Exception $e) {
            $message = "成本查詢API錯誤 Error: " . $e->getMessage();
            \Log::critical($message);
            exit($message);
        }


    }


    public function getProductBySkus($skus)
    {
        try {
            $collections = DB::connection('eg_product')->table('products')
                ->whereIn('sku', $skus)
                ->get();
        } catch (\Exception $e) {
            $message = "eg_product's connection get some problem, plz contact Frank";
            \Log::critical($message);
            exit($message);
        }

        return $collections;
    }



    public function getNewPriceV2($outer_price, $webike_cost, $product_id, $webike_prices)
    {
        $level = $this->getPriceLevel($outer_price, $webike_cost);
        $price_adjust_para = $this->getPriceAdjustPara($level);

        $webike_price_c = $webike_prices->where('role_id', 2)->firstWhere('product_id', $product_id);
        $webike_price_d = $webike_prices->where('role_id', 3)->firstWhere('product_id', $product_id);

        if ($webike_price_c === null || $webike_price_d === null) {
            $message = "product_id: $product_id, 無法查到C, D價格";
            \Log::critical($message);
        }

        switch ($level) {
            case 2:
                $c_price = ($outer_price - ($outer_price * $price_adjust_para)) ;
                return [
                    'price_level' => $level,
                    'c_price' => $c_price,
                    'd_price' => $c_price * 0.95
                ];
                break;
            case 1:
                $c_price = ($outer_price - ($outer_price * $price_adjust_para)) ;
                return [
                    'price_level' => $level,
                    'c_price' => $c_price,
                    'd_price' => $c_price * 0.95
                ];
                break;
            case 0:
                return [
                    'price_level' => $level,
                    'c_price' => $webike_price_c->price ?? -1,
                    'd_price' => $webike_price_d->price ?? -1
                ];
                break;
        }

    }

    public function getPriceLevel($outer_price, $webike_cost)
    {
        $total_cost = $webike_cost->total_cost;
        if ($total_cost == 0) return 0; //無競爭力
        if (($outer_price - $total_cost) / $total_cost > 10) return 0;  //IF > 1000%，不跟價格，依照PM2.0設定去走
        switch (true) {
            case (($outer_price - $total_cost) / $total_cost) >= 0.2:
//                有競爭力
                return 2;
                break;
            case (($outer_price - $total_cost) / $total_cost) >= 0.07 && (($outer_price - $total_cost) / $total_cost) < 0.2:
//                勉強有競爭力
                return 1;
                break;
            default:
                //無競爭力
                return 0;
        }
    }

    public function getPriceAdjustPara($level)
    {
        switch ($level) {
            case 2:
                return 0.03;
                break;
            case 1:
                return 0.01;
                break;
            case 0:
                return false;
                break;
            default:
                throw new \Exception('WTF');
        }
    }


    public function removeTrashString($text)
    {
        $problem_strings = [
            '☆KBN☆',
        ];
        foreach ($problem_strings as $problem_string) {
            $text = str_replace($problem_string, ' ', $text);
        }

        return $text;
    }

    public function removeSolrProblemString($text)
    {
        $problem_strings = [
            '+',
            '&&',
            '||',
            '!',
            '(',
            ')',
            '{',
            '}',
            '[',
            ']',
            '^',
            '"',
            '~',
            '*',
            '?',
            ':',
            '\\',
            '/',
            '#',
            ';',

        ];
        foreach ($problem_strings as $problem_string) {
            $text = str_replace($problem_string, ' ', $text);
        }

        return $text;
    }



    public function solrSearch($key_word, $manufacturer_id = null)
    {
        $key_word = urlencode($key_word);
        $solr_api = config('api.solr_api');
        try {
            switch ($manufacturer_id) {
                case null:
                    $solr_search_result = file_get_contents($solr_api . "?q=$key_word&rows=1&wt=json&indent=true&fq=type:0");
                    break;

                default:
                    $solr_search_result = file_get_contents($solr_api . "?q=$key_word&rows=1&wt=json&indent=true&fq=type:0&fq=manufacturer_id%3A($manufacturer_id)");

            }
        } catch (\Exception $e) {
            $message = "Solr api get some problem, Error: " . $e->getMessage();
            \Log::critical($message);
            exit($message);


        }


        $json = json_decode($solr_search_result);
        return $json;
    }


    public function removeChinese($text)
    {
        $text = preg_replace("/\p{Han}+/u", '', $text);
        return $text;
    }

    function httpPost($url, $data)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
//        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    public function getProductsOptions($skus)
    {
        $sku_para = '';
        foreach ($skus as $sku) {
            $sku_para .= $sku . ',';
        }

        $client = new \GuzzleHttp\Client;
        $response = $client->request('POST', config('api.product_options'), ['form_params' => [
            'sku' => $sku_para
        ]]);
        $json_response = json_decode($response->getBody());

        return collect($json_response->data->products ?? []);
    }
}