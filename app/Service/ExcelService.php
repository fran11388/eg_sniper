<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/1/22
 * Time: 上午 10:17
 */

namespace App\Service;

use Maatwebsite\Excel\Facades\Excel;

class ExcelService
{

    public static function readFirstUploadExcel()
    {
        if(collect(request()->file())->first()->getClientOriginalExtension()!='xls'){
            return false;
        }

        try{
            $path=collect(request()->file())->first()->getRealPath();
            return self::readExcel($path);
        }catch(\Exception $e){
//            $path = collect(request()->file())->first()->store('');
//            $path = storage_path("app/$path");
//            chmod($path, 0777);
//            return self::readExcel($path);
        }
    }

    public static function readExcel($path)
    {
        $excel_rows=null;
        Excel::load($path, function ($reader) use(&$excel_rows){
            $data = $reader->get();
            $excel_rows= $data->all();
        });
        return collect($excel_rows);
    }

    public static function export($fileName, $sheetName, $cellData)
    {

        Excel::create($fileName, function ($excel) use ($cellData, $sheetName) {
            $excel->sheet($sheetName, function ($sheet) use ($cellData) {
                $sheet->rows($cellData);
            });
        })->export('xls');
    }

    public static function replace_4byte($string) /*if a string contain emoji ,must call this function to clean it*/
    {
        return preg_replace('%(?:
          \xF0[\x90-\xBF][\x80-\xBF]{2}      # planes 1-3
        | [\xF1-\xF3][\x80-\xBF]{3}          # planes 4-15
        | \xF4[\x80-\x8F][\x80-\xBF]{2}      # plane 16
    )%xs', '', $string);
    }
}