<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2019/3/29
 * Time: 上午 10:46
 */

namespace App\Service;


class Status
{
    const WAITING_CRAWL = 4; //等待抓取
    const COMPLETED = 5; //完成
    const WAITING_UPDATE = 8; //價格等待更新
    const PRICE_LEVEL_0 = 11; //價格無競爭力
    const PRICE_LEVEL_1 = 10; //價格免強有競爭力
    const PRICE_LEVEL_2 = 9;  //價格有競爭力
    const NO_MAP = 12;  //無對應商品
    const NOT_CHECK = 13; //未確認商品
}