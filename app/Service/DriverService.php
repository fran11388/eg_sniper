<?php
/**
 * Created by PhpStorm.
 * User: User2
 * Date: 2018/1/3
 * Time: 上午 11:50
 */

namespace App\Service;

use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\Remote\WebDriverCapabilityType;

use Facebook\WebDriver\Chrome\ChromeOptions;

class DriverService
{
    public static function getDriver($name)
    {

        switch ($name) {
            case 'chrome':
                $host = 'http://localhost:4444/wd/hub';
                $capabilities = DesiredCapabilities::chrome();
                break;
            case 'phantomjs':
                $host = '127.0.0.1:8910';
                $capabilities = DesiredCapabilities::phantomjs();
                break;
            case 'firefox':
                $host = 'http://localhost:4444/wd/hub';
                $capabilities = DesiredCapabilities::firefox();
                break;
            case 'chrome_headless':
                $host = 'http://localhost:4444/wd/hub';
                $capabilities = DesiredCapabilities::chrome();
                $options = new ChromeOptions();
                $options->addArguments(array(
                    '--window-size=1920,1080',
                    '--headless',
                    '--disable-gpu',
                    '--no-sandbox'
                ));
                $capabilities->setCapability(ChromeOptions::CAPABILITY, $options);
                break;
            case 'firefox_headless':
                $host = 'http://localhost:4444/wd/hub';
                $capabilities = DesiredCapabilities::firefox();
                $capabilities->setCapability(
                    'moz:firefoxOptions',
                    ['args' => ['-headless']]
                );
                break;
            default:
                $host = 'http://localhost:4444/wd/hub';
                $capabilities = DesiredCapabilities::chrome();
                $options = new ChromeOptions();
                $options->addArguments(array(
                    '--window-size=1920,1080',
                    '--headless',
                    '--disable-gpu',
                    '--no-sandbox'
                ));
                $capabilities->setCapability(ChromeOptions::CAPABILITY, $options);
        }
        return RemoteWebDriver::create($host, $capabilities, 1000 * 60);

    }

    public static function driverExecuteScript(RemoteWebDriver $driver, $script_list)
    {

        $result = null;
        foreach ($script_list as $key => $value) {
            try {
                $result = $driver->executeScript($value);
                if ($result == null) continue;
//                echo "use rule: $key<BR>";     //echo something will cause export excel error
                break;
            } catch (\Exception $e) {
                continue;
            }
        }
        return $result;
    }

    public static function driverSimulateScroll(RemoteWebDriver $driver, $delay_second)
    {

        $last_height = $driver->executeScript("return document.body.scrollHeight");
        while (true) {
            $driver->executeScript('window.scrollTo(0, document.body.scrollHeight);');
            sleep($delay_second);
            $new_height = $driver->executeScript("return document.body.scrollHeight");
            if ($new_height == $last_height) {
                break;
            }
            $last_height = $new_height;
        }
    }
}