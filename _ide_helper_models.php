<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\ExpiredItem
 *
 * @property int $id
 * @property string|null $url
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExpiredItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExpiredItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExpiredItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExpiredItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExpiredItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExpiredItem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ExpiredItem whereUrl($value)
 */
	class ExpiredItem extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\ItemChangeLog
 *
 * @property int $id
 * @property int|null $outer_item_id
 * @property string|null $name
 * @property string|null $new_name
 * @property float|null $price
 * @property float|null $new_price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ItemChangeLog newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ItemChangeLog newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ItemChangeLog query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ItemChangeLog whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ItemChangeLog whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ItemChangeLog whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ItemChangeLog whereNewName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ItemChangeLog whereNewPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ItemChangeLog whereOuterItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ItemChangeLog wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\ItemChangeLog whereUpdatedAt($value)
 */
	class ItemChangeLog extends \Eloquent {}
}

namespace App\Models\Log{
/**
 * App\Models\Log\Task
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $message
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\Task newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\Task newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\Task query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\Task whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\Task whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\Task whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\Task whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Log\Task whereUpdatedAt($value)
 */
	class Task extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\MappingProduct
 *
 * @property int $id
 * @property int|null $outer_item_id
 * @property string|null $solr_keyword
 * @property string|null $sku
 * @property int|null $product_id
 * @property string|null $webike_name
 * @property string|null $model_number
 * @property int|null $webike_price_c
 * @property int|null $webike_price_d
 * @property float|null $webike_price_c_new
 * @property float|null $webike_price_d_new
 * @property float|null $webike_min_price
 * @property string|null $webike_product_total_cost
 * @property float|null $webike_product_cost_ntd
 * @property float|null $webike_product_cost_jpy
 * @property float|null $c_price_vs_outer_price
 * @property string|null $outer_item_url
 * @property string|null $webike_item_url
 * @property float|null $webike_price_c_new_TP
 * @property float|null $webike_price_d_new_TP
 * @property float|null $webike_price_c_new_FP
 * @property float|null $webike_price_d_new_FP
 * @property float|null $new_c_price_sub_outer_price
 * @property float|null $new_c_price_sub_outer_price_div_new_c
 * @property int|null $status_id 2:對應失敗 6:應用OK 7:應用NG 8:等待更新
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property bool|null $apply_price
 * @property-read \App\Models\OuterItem|null $outerItem
 * @property-read \App\Models\Status $status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereApplyPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereCPriceVsOuterPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereModelNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereNewCPriceSubOuterPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereNewCPriceSubOuterPriceDivNewC($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereOuterItemId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereOuterItemUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereSolrKeyword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereWebikeItemUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereWebikeMinPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereWebikeName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereWebikePriceC($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereWebikePriceCNew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereWebikePriceCNewFP($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereWebikePriceCNewTP($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereWebikePriceD($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereWebikePriceDNew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereWebikePriceDNewFP($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereWebikePriceDNewTP($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereWebikeProductCostJpy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereWebikeProductCostNtd($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\MappingProduct whereWebikeProductTotalCost($value)
 */
	class MappingProduct extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\OuterItem
 *
 * @property int $id
 * @property int|null $seller_id
 * @property int|null $manufacturer_id
 * @property string|null $url
 * @property string|null $name
 * @property float|null $price_min
 * @property float|null $price_max
 * @property string|null $description
 * @property string|null $img
 * @property int|null $deprecated
 * @property int|null $status_id 1:等待對應  2:對應失敗  3:對應成功
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ItemChangeLog[] $changeLogs
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\MappingProduct[] $mappingProducts
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OuterItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OuterItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OuterItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OuterItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OuterItem whereDeprecated($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OuterItem whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OuterItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OuterItem whereImg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OuterItem whereManufacturerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OuterItem whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OuterItem wherePriceMax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OuterItem wherePriceMin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OuterItem whereSellerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OuterItem whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OuterItem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\OuterItem whereUrl($value)
 */
	class OuterItem extends \Eloquent {}
}

namespace App\Models\Regular{
/**
 * App\Models\Regular\History
 *
 * @property int $id
 * @property int|null $regular_id
 * @property int|null $product_id
 * @property int $role_id 0 -> 定價
 * 1 -> C
 * 2 -> D
 * @property float|null $price
 * @property int|null $user_id
 * @property string|null $sync_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regular\History newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regular\History newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regular\History query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regular\History whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regular\History whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regular\History wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regular\History whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regular\History whereRegularId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regular\History whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regular\History whereSyncAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regular\History whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regular\History whereUserId($value)
 */
	class History extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Regular
 *
 * @property int $id
 * @property int|null $mapping_product_id
 * @property string|null $sku
 * @property int|null $product_id
 * @property float|null $price_c
 * @property float|null $price_d
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regular newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regular newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regular query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regular whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regular whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regular whereMappingProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regular wherePriceC($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regular wherePriceD($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regular whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regular whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Regular whereUpdatedAt($value)
 */
	class Regular extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Seller
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $url
 * @property int|null $manufacturer_id
 * @property int|null $status_id 4:等待抓取 5:完成
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Seller newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Seller newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Seller query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Seller whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Seller whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Seller whereManufacturerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Seller whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Seller whereStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Seller whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Seller whereUrl($value)
 */
	class Seller extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Status
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $display_name
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Status newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Status newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Status query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Status whereDisplayName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Status whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Status whereName($value)
 */
	class Status extends \Eloquent {}
}

namespace App{
/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\User whereUpdatedAt($value)
 */
	class User extends \Eloquent {}
}

