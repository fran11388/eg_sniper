<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2018/11/28
 * Time: 下午 05:33
 */
return [
    'solr_api' => env('SOLR_API', 'http://153.120.83.216:9000/solr/webike/select'),
    'brand_query' => env('BRAND_QUERY_API', 'http://www.yuintro.com:81/api/test/query-brand'),
    'apply_price' => env('APPLY_PRICE_API', 'http://pricing.everglory.asia/api/price/fixed/edit'),
    'query_cost' => env('QUERY_COST_API', 'http://pricing.everglory.asia/api/cost'),  //ex: http://pricing.everglory.asia/api/cost?product_ids=1399134,2767161
    'product_options' => env('PRODUCT_OPTIONS_API', 'http://153.120.25.92/productStation/public/index.php/api/get-product-option'),
    'query_price_record' => env('QUERY_PRICE_RECORD_API', 'http://pricing.everglory.asia/api/price/fixed/query'),
    'delete_price' => env('DELETE_PRICE_API', 'http://pricing.everglory.asia/api/price/fixed/remove'),
    'account_receives'=>env('ACCOUNT_RECEIVES_API','http://zero.everglory.asia/api/sniper/receives'),
];
