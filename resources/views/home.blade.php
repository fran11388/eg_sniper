<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="noindex, nofollow">

    <title>Price Compete System</title>  {{--https://bootsnipp.com/snippets/featured/bootstrap-4-our-services--}}
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <style type="text/css">
        .mb-60 {
            margin-bottom: 60px;
        }

        .services-inner {
            border: 2px solid #48c7ec;
            margin-left: 35px;
            transition: .3s;
        }

        .our-services-img {
            float: left;
            margin-left: -36px;
            margin-right: 22px;
            margin-top: 28px;
        }

        .our-services-text {
            padding-right: 10px;
        }

        .our-services-text {
            overflow: hidden;
            padding: 28px 0 25px;
        }

        .our-services-text h4 {
            color: #222222;
            font-size: 18px;
            font-weight: 700;
            letter-spacing: 1px;
            margin-bottom: 8px;
            padding-bottom: 10px;
            position: relative;
            text-transform: uppercase;
        }

        .our-services-text h4::before {
            background: #ec6d48 none repeat scroll 0 0;
            bottom: 0;
            content: "";
            height: 1px;
            position: absolute;
            width: 35px;
        }

        .our-services-wrapper:hover .services-inner {
            background: #fff none repeat scroll 0 0;
            border: 2px solid transparent;
            box-shadow: 0px 5px 10px 0px rgba(0, 0, 0, 0.2);
        }

        .our-services-text p {
            margin-bottom: 0;
        }

        p {
            font-size: 14px;
            font-weight: 400;
            line-height: 26px;
            color: #666;
            margin-bottom: 15px;
        }    </style>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script type="text/javascript">
        window.alert = function () {
        };
        var defaultCSS = document.getElementById('bootstrap-css');

        function changeCSS(css) {
            if (css) $('head > link').filter(':first').replaceWith('<link rel="stylesheet" href="' + css + '" type="text/css" />');
            else $('head > link').filter(':first').replaceWith(defaultCSS);
        }

        $(document).ready(function () {
            var iframe_height = parseInt($('html').height());
            window.parent.postMessage(iframe_height, 'https://bootsnipp.com');
        });
    </script>
</head>
<body>
<div class="container">
    {{ Breadcrumbs::render('home') }}
    <h2>Price Compete System</h2>


    <div class="row">
        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
            <div class="our-services-wrapper mb-60">
                <a href="{{route('get_brand_list')}}">
                    <div class="services-inner">
                        <div class="our-services-img">
                            <img src="{{asset('image/ecological-icon.png')}}"
                                 width="68px" alt="">
                        </div>
                        <div class="our-services-text">
                            <h4>品牌清單</h4>
                            <p>查看各品牌與對手商品之價格比較</p>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
            <div class="our-services-wrapper mb-60">
                <div class="services-inner">
                    <div class="our-services-img">
                        <img src="{{asset('image/ecological-icon.png')}}"
                             width="68px" alt="">
                    </div>
                    <div class="our-services-text">
                        <h4>未開發</h4>
                        <p>未開發</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12">
            <div class="our-services-wrapper mb-60">
                <div class="services-inner">
                    <div class="our-services-img">
                        <img src="{{asset('image/ecological-icon.png')}}"
                             width="68px" alt="">
                    </div>
                    <div class="our-services-text">
                        <h4>未開發</h4>
                        <p>未開發</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
</script>
</body>
</html>
