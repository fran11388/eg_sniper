<!DOCTYPE html>
<html lang="en">
<head>
    <title>Price Compete System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

    {{--select 2--}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#add_raw').on('click', function () {

                var html = "<input type=\"text\" class=\" col-3\" placeholder=\"名稱\" name=\"seller_names[]\">\n" +
                    "                <input type=\"text\" class=\" col-6\" placeholder=\"網址\"   name=\"seller_urls[]\">\n" +
                    "                <button onclick=\"removeElement(this.parentNode.getAttribute('id'))\"  type=\"button\" class=\"btn btn-secondary\">清空</button>";

                addElement('list_group', 'div', 'delete-key-' + Math.random(), html);
            });

            $(".js-data-brand-ajax").select2({
                ajax: {
                    url: "{{config('api.brand_query')}}",
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        // console.log(params);
                        return {
                            keyword: params.term, // search term
                        };
                    },
                    processResults: function (data, params) {
                        // parse the results into the format expected by Select2
                        // since we are using custom formatting functions we do not need to
                        // alter the remote JSON data, except to indicate that infinite
                        // scrolling can be used
                        console.log(data);
                        options = [];
                        for (i = 0; i < data.length; i++) {
                            options.push({
                                'id': data[i].id,
                                'text': data[i].title,
                            });
                        }

                        return {
                            results: options,
                        };
                    },
                    // cache: true
                },
                placeholder: '輸入品牌關鍵字',
                // escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
                minimumInputLength: 1,
                // templateResult: formatRepo,
                // templateSelection: formatRepoSelection
            });

        });

        function addElement(parentId, elementTag, elementId, html) {
            // Adds an element to the document
            var p = document.getElementById(parentId);
            var newElement = document.createElement(elementTag);
            newElement.setAttribute('id', elementId);
            newElement.innerHTML = html;
            p.appendChild(newElement);
        }

        function removeElement(elementId) {
            // Removes an element from the document
            var element = document.getElementById(elementId);
            element.parentNode.removeChild(element);
        }
    </script>

</head>
<body>

<div class="container mt-3">


    @if(isset($brand))
        <p style="background:grey;color:white">{{$brand->name}} 品牌設定</p>
    @else
        <p style="background:grey;color:white">品牌新增</p>
    @endif


    <form method="post" action="{{route('post_brand_edit')}}">
        @csrf

        <label for="">請選擇品牌：</label>


        @if(isset($brand))
            <select class="custom-select custom-select-sm mb-3 col-sm-6" name="brand_id">
                <option selected value="{{$brand->id}}">{{$brand->name}}</option>
            </select>
            <input name="mode" value="edit" hidden>
        @else
            <select class="custom-select custom-select-sm mb-3 col-sm-6 js-data-brand-ajax" name="brand_id"></select>
        @endif

        <br>
        <label for="">請輸入對手賣場資料(<a href="{{asset('doc/sellers_setting.pdf')}}" class="" role="button" target="_blank">設定參考</a>) :</label>
        <br>

        <div id="list_group">


            @if(isset($brand))
                @foreach($sellers as $seller)
                    <div id="delete-key-{{rand(1,1000000) }}">
                        <input type="text" class=" col-3" placeholder="名稱" name="seller_names[]" value="{{$seller->name}}">
                        <input type="text" class=" col-6" placeholder="網址" name="seller_urls[]" value="{{$seller->url}} (商品數:{{$seller->items->count()}})" style=" @if($seller->status_id==\App\Service\Status::COMPLETED)
                                background: #28a74578;
                        @endif" disabled>
                        {{--disable's input will not be submit, add a hidden input--}}
                        <input type="text" class=" col-6" placeholder="網址" name="seller_urls[]" value="{{$seller->url}} " hidden>
                        <button onclick="removeElement(this.parentNode.getAttribute('id'))" type="button"
                                class="btn btn-secondary">清空
                        </button>
                    </div>
                @endforeach
            @endif

            <div id="delete-key-{{rand(1,1000000) }}">
                <input type="text" class=" col-3" placeholder="名稱" name="seller_names[]">
                <input type="text" class=" col-6" placeholder="網址" name="seller_urls[]">
                <button onclick="removeElement(this.parentNode.getAttribute('id'))" type="button"
                        class="btn btn-secondary">清空
                </button>
            </div>
        </div>


        <button id="add_raw" type="button" class="btn btn-primary">新增欄位</button>
        <br><br><br>
        <button type="submit" class="btn btn-primary">保存</button>
        <a href="{{route('get_brand_list')}}" class="btn btn-warning" role="button">回上一頁</a>



    </form>
</div>

</body>
</html>
