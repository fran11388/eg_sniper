<!DOCTYPE html>
<html lang="en">
<head>
    <title>Price Compete System</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>


</head>
<body>

<div class="container">
    {{--<h2></h2>--}}
    {{--<p>The .table-sm class makes the table smaller by cutting cell padding in half:</p>--}}
    {{ Breadcrumbs::render('brand_panel',$brand_id) }}
    {{--<table class="table table-bordered table-sm">--}}
    {{--<thead>--}}
    {{--<tr>--}}
    {{--<th colspan="2">最後比對時間：{{$last_mapping_time}}</th>--}}
    {{--<th>商品數量</th>--}}
    {{--<th>應用狀態</th>--}}
    {{--<th>檢查人</th>--}}
    {{--</tr>--}}
    {{--</thead>--}}
    {{--<tbody>--}}
    {{--<tr>--}}
    {{--<td rowspan="2">比對成功</td>--}}
    {{--<td>應用OK</td>--}}
    {{--<td><a href="{{route('mapping_result').'/'.$brand_id.'?type=ok'}}">{{$num_of_apply_ok}}</a></td>--}}
    {{--<td>未應用 / 應用完成</td>--}}
    {{--<td>Harry</td>--}}
    {{--</tr>--}}

    {{--<tr>--}}

    {{--<td>應用NG</td>--}}
    {{--<td><a href="{{route('mapping_result').'/'.$brand_id.'?type=ng'}}">{{$num_of_apply_ng}}</a></td>--}}
    {{--<td>--</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}

    {{--<tr>--}}
    {{--<td rowspan="2">比對失敗</td>--}}
    {{--<td>WEBIKE有的商品</td>--}}
    {{--<td><a href="{{route('mapping_fail').'/'.$brand_id.'?type=webike'}}">{{$num_of_webike_mapping_fail}}</a></td>--}}
    {{--<td>--</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}

    {{--<tr>--}}

    {{--<td>對手有的商品</td>--}}
    {{--<td><a href="{{route('mapping_fail').'/'.$brand_id.'?type=other'}}">{{$num_of_other_mapping_fail}}</a></td>--}}
    {{--<td>--</td>--}}
    {{--<td></td>--}}
    {{--</tr>--}}
    {{--<tr>--}}
    {{--<td>對應狀況 (已對應數/外部商品數/等待SKU更新數)</td>--}}
    {{--<td>{{$mapping_count}}/{{$outer_items_count}}/{{$waiting_sku_update_count}}</td>--}}
    {{--<td>--</td>--}}
    {{--<td>--</td>--}}
    {{--<td>--</td>--}}
    {{--</tr>--}}
    {{--</tbody>--}}
    {{--</table>--}}

    @php
        $tack_crawl=\App\Models\Log\Task::where('title',\App\Constants\Command::CRAWL_SELLER)->first();
    $task_apply_price=\App\Models\Log\Task::where('title',\App\Constants\Command::APPLY_PRICE)->first();
    @endphp


    {{--V2--}}
    <a href="{{route('get_import_excel')}}" class="btn btn-warning" target="_blank"> <span>匯入</span></a>
    <a href="{{route('export_excel').'/'.$brand_id}}" class="btn btn-success">
        <span>匯出</span></a>

    <table class="table table-bordered table-sm">
        <thead>
        <tr>
            <th colspan="2">最後撈取時間：{{$tack_crawl->updated_at??'--'}}</th>
            <th>商品數量</th>
            <th>目前狀態</th>
            <th>檢查人</th>
            <th>上次應用日期</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td rowspan="7">確認成功</td>
            <td rowspan="2"><a href="{{route('mapping_result')."?brand_id=$brand_id&status_id=9"}}">有競爭力</a> <a
                        href="{{route('get_mapping_edit')."?brand_id=$brand_id"."&status_id=9"}}">Edit</a></td>
            <td>{{$brand_info->level_2_no_apply}}</td>
            <td>未應用</td>
            <td rowspan="2"></td>
            <td rowspan="2">{{$task_apply_price->updated_at??'--'}}</td>
        </tr>
        <tr>
            <td>{{$brand_info->level_2_apply}}</td>
            <td>應用完成</td>
        </tr>
        <tr>
            <td rowspan="2"><a href="{{route('mapping_result')."?brand_id=$brand_id&status_id=10"}}">勉強有競爭力</a> <a
                        href="{{route('get_mapping_edit')."?brand_id=$brand_id"."&status_id=10"}}">Edit</a></td>
            <td>{{$brand_info->level_1_no_apply}}</td>
            <td>未應用</td>
            <td rowspan="2"></td>
            <td rowspan="2">{{$task_apply_price->updated_at??'--'}}</td>
        </tr>
        <tr>
            <td>{{$brand_info->level_1_apply}}</td>
            <td>應用完成</td>
        </tr>
        <tr>
            <td rowspan="2"><a href="{{route('mapping_result')."?brand_id=$brand_id&status_id=11"}}">無競爭力</a> <a
                        href="{{route('get_mapping_edit')."?brand_id=$brand_id"."&status_id=11"}}">Edit</a></td>
            <td>{{$brand_info->level_0_no_apply}}</td>
            <td>未應用</td>
            <td rowspan="2"></td>
            <td rowspan="2">{{$task_apply_price->updated_at??'--'}}</td>
        </tr>

        <tr>
            <td>{{$brand_info->level_0_apply}}</td>
            <td>應用完成</td>
        </tr>

        <tr>
            <td><a href="{{route('get_mapping_edit')."?brand_id=$brand_id"."&status_id=8"}}">等待更新</a></td>
            <td>{{$brand_info->waiting_update}}</td>
            <td>--</td>
            <td>--</td>
            <td>--</td>
        </tr>

        <tr>
            <td rowspan="3">未確認</td>
            <td><a href="{{route('get_mapping_edit')."?brand_id=$brand_id"."&status_id=13"}}">未確認商品</a></td>
            <td>{{$brand_info->non_check}}  </td>
            <td>未確認完成，等待確認</td>
            <td>Someone</td>
            <td></td>
        </tr>
        <tr>
            <td><a href="{{route('get_mapping_edit')."?brand_id=$brand_id"."&status_id=12"}}">無對應商品</a></td>
            <td> {{$brand_info->non_correspond}}</td>
            <td>已確認，我們無對應商品</td>
            <td>Someone</td>
            <td></td>
        </tr>

        <tr>
            <td><a href="#">WEBIKE剩餘的商品</a></td>
            <td>{{$num_of_webike_mapping_fail}}</td>
            <td>--</td>
            <td>--</td>
            <td>--</td>
        </tr>

        <tr>
            <td colspan="1">賣家</td>
            <td colspan="5">{{$brand_info->num_of_seller}}/{{$brand_info->non_finish_seller}} (賣家數/未完成)</td>
        </tr>

        </tbody>
    </table>
</div>

</body>
</html>
