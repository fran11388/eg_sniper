<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <p>更新日期：{{date('Y-m-d')}}</p>
    <p>更新內容如下</p>
    <br>
    <p>新增對手品牌</p>
    <p>--------------------------------------------------------------------------------------------------</p>
    <p>昨日新增對應品牌：{{$query->昨日新增品牌}}</p>
    <p>目前Total對應品牌：{{$query->目前Total品牌}}</p>
    <br>
    <p>新增對應商品數</p>
    <p>--------------------------------------------------------------------------------------------------</p>
    <p>昨日新增對應商品數：{{$query->昨日新增對應商品數}}</p>
    <p>目前Total對應商品數：{{$query->目前Total對應商品數}}</p>
    <br>
    <p>競爭力確認</p>
    <p>--------------------------------------------------------------------------------------------------</p>
    <p> 有競爭力商品數：{{$query->有競爭力商品數}}</p>
    <p>免強有競爭力商品數：{{$query->免強有競爭力商品數}}</p>
    <p>無競爭力商品數：{{$query->無競爭力商品數}}</p>
    <br>
    <p> 等待更新</p>
    <p> --------------------------------------------------------------------------------------------------</p>
    <p> 待確認 對手剩餘商品數：{{$query->未確認}}</p>

</div>

</body>
</html>
